/*

  tilt.js
  =======

  Contributors: Michael Sasser
  Maintainer: Michael Sasser
  License: MIT
  Version: 1

*/

import VanillaTilt from 'vanilla-tilt';

VanillaTilt.init(document.querySelectorAll('.hero-image-tilt'), {
    max: 0.5,
    speed: 1500,
    reverse: true,
    'mouse-event-element': '.home',
});

VanillaTilt.init(document.querySelectorAll('.card-tilt'), {
    max: 0.2,
    reverse: true,
    speed: 800,
    scale: 1.05,
    glare: true,
    'max-glare': 0.03,
    perspective: 1000,
});
