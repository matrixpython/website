/*

  Chart Shortcode
  =============

  Contributors: Michael Sasser
  Maintainer: Michael Sasser
  License: MIT
  Version: 1
  Child Shortcodes: None
  Parent Shortcodes: None

  LAYOUT: /layouts/shortcodes/chart.html
  STYLE:  /assets/scss/components/_shortcode_chart.scss
  DOCS:   /content/<language>/wiki/Help:Shortcode_Chart.md
  LIB:    chart.js
  SCRIPT: /assets/js/chart.js

*/

import {Chart, registerables} from 'chart.js';
import 'chartjs-adapter-moment';
import annotationPlugin from 'chartjs-plugin-annotation';


Chart.register(...registerables, annotationPlugin);

export * from 'chart.js';
export default Chart;

window.Chart = Chart;

// import {
//   Chart,
//   Colors,
//   BarController,
//   CategoryScale,
//   LinearScale,
//   BarElement,
//   Legend
// } from 'chart.js'

// Chart.register(
//   Colors,
//   BarController,
//   BarElement,
//   CategoryScale,
//   LinearScale,
//   Legend
// );


