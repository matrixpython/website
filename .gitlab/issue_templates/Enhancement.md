<!-- 
  Thank you for helping to improve in the community website.
  Please fill in as much of the template below as you can.
-->

### Feature motivation
<!-- A clear and concise description of the problem or missing capability. -->


### Describe a solution you'd like
<!-- A description of what you want to happen. -->


### Describe the alternatives you've considered
<!-- Let us know about other (alternative) solutions you've tried or researched. -->


### Additional context
<!-- Add any other context or screenshots about the feature request here. -->


### Code of Conduct
<!-- To submit this issue, you need to agree to follow our Code of Conduct. -->

- [ ] I agree to follow this project's [Code of Conduct](https://matrix-python.github.io/docs/contributing/code_of_conduct/)
