<div align="center">

  <!-- Keep the empty line between the div and this line on GitLab -->
  <br />
  <a href="https://matrixpython.org/">
    <img src="https://gitlab.com/matrixpython/website/-/raw/main/static/favicon.svg"
         alt="MPWebsite"
         width="200"
    >
    </img>
  </a>
  <br />
  <h1>Python Community on Matrix Website</h1>
  <br />

<h4>
  We are the Python Community on Matrix, a free and open network for secure,
  decentralized communication.
</h4>

  https://matrixpython.org/

<p>
  <a href="https://gitlab.com/matrixpython/website/-/blob/main/LICENSE.md">
    <img alt="License code"
         src="https://img.shields.io/badge/license%20code-MIT-blue"
    >
    </img>
  </a>
  <a href="https://gitlab.com/matrixpython/website/-/blob/main/LICENSE_DOCS.md">
    <img alt="License docs"
         src="https://img.shields.io/badge/license%20docs-CC%20BY%204.0-blue"
    >
    </img>
  </a>
  <a href="https://matrixpython.org/">
    <img alt="Website"
         src="https://img.shields.io/website?url=https%3A%2F%2Fmatrixpython.org%2F"
    >
    </img>
  </a>
  <a href="https://website/contributors/">
    <img alt="GitLab contributors"
         src="https://img.shields.io/gitlab/contributors/matrixpython/website"
    >
    </img>
  </a>
  <a href="https://gitlab.com/matrixpython/website/-/commits/main">
    <img alt="GitLab last commit"
         src="https://img.shields.io/gitlab/last-commit/matrixpython/website"
    >
  </a>
</p>

<p>
  <a href="#website-status">Website Status</a> •
  <a href="#disclaimer">Disclaimer</a> •
  <a href="#contributing">Contributing</a> •
  <a href="#contact">Contact</a> •
  <a href="#license">License</a>
</p>

![screenshot](https://gitlab.com/matrixpython/website/-/raw/main/static/screenshot_landing.png)

</div>

## Website Status

This website is still in development.

You can find the latest build on
[https://matrixpython.org](https://matrixpython.org).

### Disclaimer

This project has not been released yet.
The information you find on it might be incomplete, or incorrect.
The current state does not represent the user experience in the
final stage. We are no professional web developers, and we do that in our spare
time, together with other projects and the moderation of the community.
So it might take a little longer until we release the page.
But we will get there.

## Contributing

If you found a defect, or you want to get involved with the development of our
website, check our [Contributing Wiki Page](https://matrixpython.org/wiki/Help:Introduction/). There you will find
all necessary information to get started, how our triage process works and
the documentation.

## Contact

We have prepared an
[FAQ section](https://matrixpython.org/docs/help/faq/), which might
answer your questions. If not, simply
[contact us](https://matrixpython.org/contact/).

## License

Designed by the Python Community on Matrix team with the help of our
[contributors](https://matrixpython.org/contributors/) ❤️.<br />
Based on [Doks](https://getdoks.org/), built with
[Hugo](https://gohugo.io/).<br />
Code licensed
[MIT](https://gitlab.com/matrixpython/website/blob/master/LICENSE.md),
docs
[CC BY 4.0](https://gitlab.com/matrixpython/website/blob/master/LICENSE_DOCS.md).

The license of this repositories logo: https://gitlab.com/matrixpython/designs/-/blob/main/gitlab_logos/website_logo
