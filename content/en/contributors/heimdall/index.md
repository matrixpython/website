---
title: "Heimdall"
description: "Member of the moderation team"
lead: "Hi, I'm Heimdall, a moderation bot to protect the community."
date: 2023-01-01T00:02:00+01:00
lastmod: 2023-01-01T00:02:00+01:00
draft: false
images: ["heimdall_ava.svg"]
avatar: "heimdall_ava.svg"
matrix_moderator: true
bot: true
---

<div style = "text-align: left">

Hi, I'm [Heimdall](https://gitlab.com/matrixpython/heimdall), the
**all-seeing** and **all-hearing** guardian sentry of **Asgard**.

You probably know me from films like **Thor**, **Thor 2**, **Thor 3**, and
**Thor 4**.

As you've probably heard, the rainbow bridge **Bifröst** was destroyed, so I
had to apply for a desk job. I'm now protecting the Python Community on Matrix
with my unique abilities.

I'm online 24/7 and part of the moderation team. Please don't try contacting
me, since I'm not designed to answer or forward questions. When you see me
writing something, it is probably one of the moderators having some fun.

If you want to report issues, check out the section "Reporting Issues" below.

## What Heimdall does

Heimdall is an advanced, feature rich moderation bot for large Matrix
communities. The bot is written by
[Michael]({{< ref "/contributors/michael-sasser" >}}) specifically for the
needs of the Python Community on Matrix. As one of the biggest pure-Matrix
communities, it has become difficult to counteract spam and scams. The tools
available at the time the bot was created had major flaws and only basic
protections. Heimdall is using statistics and Machine Learning to get the
malicious actors under control, with less work for the moderators. In addition
it is highly configurable and gives users the ability to help with.
the moderation workload.

## Reporting Issues

If you experience an issue with our services, or you encounter any problem,
that can be discussed in public, use our Meta Room
{{< identifier "#python-meta:matrix.org" >}}.

{{< reporting_issues >}}
