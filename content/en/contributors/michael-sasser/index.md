---
title: "Michael Sasser"
pronouns: "he/him"
description: "Member of the moderation team"
lead: "Hi, I'm Michael, a member of the moderation team."

date: 2020-12-31T00:00:00+02:00
lastmod: 2021-10-10T00:00:00+00:00

images: ["michael_sasser_ava.svg"]
avatar: "michael_sasser_ava.svg"

matrix_identifier: "@michael:michaelsasser.org"
matrix_username: "Michael Sasser"

email_address: "michael@matrixpython.org"
gitlab_username: "MichaelSasser"
github_username: "MichaelSasser"
mastodon_profile_url: "https://fosstodon.org/@michaelsasser"
gitea_profile_url: "https://gitea.io/en-us/" # TODO: Delete
website_url: "https://MichaelSasser.org"

bot: false
matrix_moderator: true
website_content: true
website_developer: true

draft: false
---

I'm a little bit over 30 years old and I'm from Rhineland-Palatinate, Germany.
I never quite know what to tell about myself, as I'm fairly boring.
I like to watch a film or two, preferably fantasy or sci-fi and I listen
to rock and metal all day. I'm into electronics and programming.

## Why I've created the Python room/community on Matrix

I've created the Python room on Matrix because the room, which was already
there, was abandoned by their moderator and filled up with spam. Later on
the old Python room was closed by the Matrix team and users started to join
the newly created room.

You can find more details in our "Changelog".

{{< ln "changelog" >}}

## How I got into Programming

I actually got into programming through gaming. In 2004, when I was still
young, we hosted numerous LAN parties with friends and played round-based
first-person shooters, in basements and barns, all night long. At some point,
we all had internet access and could additionally play from the comfort
of our homes at any time. Renting game servers directly was too expensive.
That's why I decided to rent a dedicated server with Debian, the first
Linux distribution I used.
That server was not very powerful, so a graphical interface for setting
up the services was out of the question. So, I learned to use Linux with man
pages and some help in forums.
Over time, the server grew, and I added an e-mail service,
a website and a forum to various game and communication servers.
Because the maintenance effort was not insignificant, I got into writing
bash scripts. However, bash scripts easily become confusing with
increasing complexity, I decided to look for another solution.
Accordingly, I started to learn Java, which I learned about through TV
commercials. Yeah, I know... Java. It seems I was young and quite misdirected.

Although I didn't like Java very much, I was able to solve many problems
with it. However, I've always tried out other languages along the way.

## How I got into Python

In 2008, I got in touch with Python from time to time. Python as a programming
language was easier to write, more readable, and the community around the
language was growing "exponentially". So, I decided to give it a try.
While learning it, I re-implemented most of the programs, I wrote in C,
Java, and Perl in Python 2.6, and I loved it.

At first, I wasn't a big fan of Python 3, when it came out. But it got better
over time. I switched when Python 3.3 was released towards the end
of 2012 and I never looked back.

## Which Programming Languages I Use

Over the years I stumbled across many languages, mostly out of curiosity, but
I've always stuck to Python, Rust, C, and some good old bash scripting.
Most of the languages I tried out for a few weeks or months, I forgot faster
then I learned them. I could probably still read them, but
writing would be a whole other subject.
Python is my main programming language. Rust replaced C almost entirely.
Only for embedded systems, I still use it.

## Contact

If you have any questions, feel free to contact me via one of the links below
my avatar.
