---
title: "Help:Shortcode Relref"
url: "/wiki/Help\\:Shortcode_Relref"
description: ""
lead: "Use relative permalinks for your links."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Relref
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: Built-in
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`relref`"
    - type: fulldata
      label: Short Description
      value: |
        Create a relative permalink to a resource.
        HTML figure.
    - type: label
      label: Hugo Documentation
      value: "[Shortcodes](https://gohugo.io/content-management/shortcodes/#ref-and-relref)"
---

`relref` is a built-in shortcode defined by Hugo using the
[ref function](https://gohugo.io/functions/relref/).

The `relref` shortcode can be used to create permalinks with an relative path,
to a resource using either an absolute or relative path.

For more information, check
[Hugo -- Links and Cross References →](https://gohugo.io/content-management/cross-references/)

## Parameters

The `relref` shortcode has the following parameters:

| Parameter | Description                                                                                                                                                                                      |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `[0]`     | The path to a page, with or without a file extension, with or without an anchor. A path without a leading `/` is first resolved relative to the given context, then to the remainder of the site |

## Examples

### From an absolute path:

```md
{{</* relref "/wiki/Help:Shortcode_Ref" */>}}<br />
{{</* relref "/wiki/Help:Shortcode_Ref.md" */>}}<br />
{{</* ref "/wiki/Help:Shortcode_Ref.md#Examples" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< relref "/wiki/Help:Shortcode_Ref" >}} <br />
{{< relref "/wiki/Help:Shortcode_Ref.md" >}} <br />
{{< relref "/wiki/Help:Shortcode_Ref.md#Examples" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

### From a relative path:

```md
{{</* relref "Help:Shortcode_Ref" */>}}<br />
{{</* relref "Help:Shortcode_Ref.md" */>}}<br />
{{</* relref "Help:Shortcode_Ref.md#Examples" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< relref "Help:Shortcode_Ref" >}} <br />
{{< relref "Help:Shortcode_Ref.md" >}} <br />
{{< relref "Help:Shortcode_Ref.md#Examples" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

### In Markdown Links

```md
[shortcodes]({{</* relref "shortcodes" */>}})
[Image Conventions]({{</* relrel "shortcodes#image-conventions" */>}})
```

### In HTML Links

```md
<a href="{{</* relref "shortcodes" */>}}"Shortcodes</a>
<a href="{{</* relref "shortcodes#image-conventions" */>}}">Image Conventions</a>
```

HTML links are normally only used in HTML documents or nested shortcodes, like
the following `warning`, where you can't use Markdown.

```md
{{</* alert */>}}
A link inside an alert: <a href="{{</* relref "wiki/Help:Workflow.md" */>}}">Workflow</a>.
{{</* /alert */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< alert >}}
A link inside an alert: <a href="{{< relref "wiki/Help:Workflow.md" >}}">Workflow</a>.
{{< /alert >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->
