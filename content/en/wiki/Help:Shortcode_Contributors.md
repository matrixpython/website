---
title: "Help:Shortcode Contributors"
url: "/wiki/Help\\:Shortcode_Contributors"
description: ""
lead: "Display a table of contributors with their avatar in front of their names."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Contributors
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`contributors`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display a list of contributors with an avatar in front of their names.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

The `contributors` shortcode renders a table of all contributors from the data
entered in the front matter of their contributor page.

For more information on how to create a contributor, check out the
Contributor documentation.

{{< ln "wiki/Help:Contributors" >}}

## Parameters

The `contributors` shortcode has no parameters.

## Examples

```md
{{</* contributors */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
  {{< contributors >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/contributors.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/contributors.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Contributors Shortcode" path="/layouts/shortcodes/contributors.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_contributors.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_contributors.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Contributors Style" path="/assets/scss/components/_shortcode_contributors.scss" >}}
