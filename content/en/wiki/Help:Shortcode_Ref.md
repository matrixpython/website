---
title: "Help:Shortcode Ref"
url: "/wiki/Help\\:Shortcode_Ref"
description: ""
lead: "Use absolute permalinks for your links."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Ref
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: Built-in
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`ref`"
    - type: fulldata
      label: Short Description
      value: |
        Create an absolute permalink to a resource.
        HTML figure.
    - type: label
      label: Hugo Documentation
      value: "[Shortcodes](https://gohugo.io/content-management/shortcodes/#ref-and-relref)"
---

`ref` is a built-in shortcode defined by Hugo using the
[ref function](https://gohugo.io/functions/ref/).

The `ref` shortcode can be used to create permalinks with an absolute path, to
a resource using either an absolute or relative path.

For more information, check
[Hugo -- Links and Cross References →](https://gohugo.io/content-management/cross-references/)

## Parameters

The `ref` shortcode has the following parameters:

| Parameter | Description                                                                                                                                                                                       |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `[0]`     | The path to a page, with or without a file extension, with or without an anchor. A path without a leading `/` is first resolved relative to the given context, then to the remainder of the site. |

## Examples

### From an absolute path:

```md
{{</* ref "/wiki/Help:Shortcode_Ref" */>}}<br />
{{</* ref "/wiki/Help:Shortcode_Ref.md" */>}}<br />
{{</* ref "/wiki/Help:Shortcode_Ref.md#Examples" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< ref "/wiki/Help:Shortcode_Ref" >}} <br />
{{< ref "/wiki/Help:Shortcode_Ref.md" >}} <br />
{{< ref "/wiki/Help:Shortcode_Ref.md#Examples" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

### From a relative path:

```md
{{</* ref "Help:Shortcode_Ref" */>}}<br />
{{</* ref "Help:Shortcode_Ref.md" */>}}<br />
{{</* ref "Help:Shortcode_Ref.md#Examples" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< ref "Help:Shortcode_Ref" >}} <br />
{{< ref "Help:Shortcode_Ref.md" >}} <br />
{{< ref "Help:Shortcode_Ref.md#Examples" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

### In Markdown Links

```md
[shortcodes]({{</* ref "shortcodes" */>}})
[Image Conventions]({{</* rel "shortcodes#image-conventions" */>}})
```

### In HTML Links

```md
<a href="{{</* ref "shortcodes" */>}}"Shortcodes</a>
<a href="{{</* ref "shortcodes#image-conventions" */>}}">Image Conventions</a>
```

HTML links are normally only used in HTML documents or nested shortcodes, like
the following `warning`, where you can't use Markdown.

```md
{{</* alert */>}}
A link inside an alert: <a href="{{</* ref "wiki/Help:Workflow.md" */>}}">Workflow</a>.
{{</* /alert */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< alert >}}
A link inside an alert: <a href="{{< ref "wiki/Help:Workflow.md" >}}">Workflow</a>.
{{< /alert >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->
