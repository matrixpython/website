---
title: "Help:Contributors"
url: "/wiki/Help\\:Contributors"
description: ""
lead: "Create a contributor's profile page."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

Whether moderator, content creator or developer, everyone who contributes to
the website needs a personal profile page on our website.
They are used in different ways. May it be a table of the contributors shown
on the website or the metadata to see, which person contributed to which
content.

For example in the header of the blog entry, the contributor's name (`title`)
is needed to show the reader more about the person who wrote the blog entry.

{{<figure caption="Blog Card from the \"Home\" page" >}}
{{< post title="Spaces Announcement 🎉" linked="false" container="false" >}}
{{< /figure >}}

The above example shows an entry, which features multiple contributors. To add
the contributors to the blog entry the `contributors` variable is used. In this
case, it was:
`contributors: ["Michael Sasser", "BrenBarn", "ilex", "James Belchamber"]`

If you click on one of the contributors, marked with a red underline, you will
see the contributor page of this user. For example,
[Michael's](https://matrixpython.gilab.io/contributors/michael-sasser/)
contributor page is located in
[`./content/contributors/michael-sasser/`](https://gitlab.com/matrixpython/matrixpython.gilab.io/tree/master/content/contributors/michael-sasser/).
It contains a file `_index.md` and a file `michael_sasser_ava.svg`:

- `_index.md` is the file, which contains all text and references the image
  used.
- `michael_sasser_ava.svg` is the image used in the contributor page. Please
  remember our [image conventions]({{< ref "wiki/Help:Introduction.md#shortcodes" >}}).
  The filename convention in those images is `my_user_ava.extention`.
  In case the avatar was not set, but the `matrix_identifier` was, the avatar
  which was set in the Python room on Matrix will be used.

## Location

| Directory                                                            | Description                            |
| -------------------------------------------------------------------- | -------------------------------------- |
| [content/\<Language\>/contributors/]({{< reporef "contributors" >}}) | The base directory of the contributors |

{{% newpage name="contributors" %}}

## Front Matter

{{< alert type="success" >}}
The `title` in this case would be your name or your nickname preferably the
on you use on Matrix.
{{< /alert >}}

{{< variable_structure "Contributor" "title" "description" "date" "lastmod" "draft" "images" "math" "avatar" "matrix_identifier" "matrix_username" "matrix_moderator" "website_content" "website_developer" "email_address" "gitlab_username" "github_username" "gitea_profile_url" "mastodon_profile_url" "website_url" "bot" >}}

Make sure to create your profile page, when you contribute to the
website. You don't actually need to enter personal information about yourself.
You can just enter your GitLab username and your matrix identifier. For the
title your name is mandatory, but nickname would be sufficient too.

## Code

Below you find the implementation of the layout.

### HTML

#### Single Page Layout

Defined in `layouts/contributors/single.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/contributors/single.html" %}}

#### List Page Layout

Defined in `layouts/contributors/list.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/contributors/list.html" %}}

### Archetype

Defined in `archetypes/contributors.md`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/archetypes/contributors.md" type="yaml" %}}
