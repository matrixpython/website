---
title: "Help:Shortcode Variable Structure"
url: "/wiki/Help\\:Shortcode_Variable_Structure"
description: ""
lead: "Document the section structure."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Variable Structure
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`variable_structure`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Document the front matter of the different sections of the homepage.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

The `variable_structure` is used in the [Section
Documentation]({{< ref "wiki/Help:Introduction#section-documentation" >}}) section. It renders a full
description of the front matter of a page. It comes with a heading line, some
descriptions, a table of variables and there descriptions, in addition how to
use the datetime string (if needed) and some generated example.

## Parameters

The post shortcode has the following parameters:

| Parameter | Description      |
| --------- | ---------------- |
| `1`       | The section name |
| `2..n`    | The variables    |

## Examples

```md
{{</* variable_structure "Foo" "title" "description" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
  {{< variable_structure "Foo" "title" "description" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/variable_structure.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/variable_structure.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Variable Structure Shortcode" path="/layouts/shortcodes/variable_structure.html" >}}
