---
title: "Help:Changelog"
url: "/wiki/Help\\:Changelog"
description: ""
lead: "Create a changelog entry."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

The "Community Changelog" shows the history of our community.
Every entry is added as its own page, but will be rendered as one linear
timeline.

## Location

| Directory                                                                         | Description                               |
| --------------------------------------------------------------------------------- | ----------------------------------------- |
| [content/\<Language\>/changelog/]({{< reporef "changelog" >}})                    | The "Changelog" entries of the about page |
| [content/\<Language\>/changelog/_index.md]({{< reporef "changelog/_index.md" >}}) | The upper part of the _Changelog_ page    |

{{% newpage name="changelog" %}}

## Front Matter

{{< variable_structure "Changelog" "title" "date" "lastmod" "contributors" "draft" "blog_post" >}}

## Code

Below you find the implementation of the layout.

### HTML

#### List Page Layout

Defined in `layouts/changelog/list.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/changelog/list.html" %}}

### SASS

Defined in `assets/scss/components/_timeline.scss`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="assets/scss/components/_timeline.scss" %}}

### Archetype

Defined in `layouts/wiki.md`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/archetypes/changelog.md" type="yaml" %}}
