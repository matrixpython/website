---
title: "Help:Shortcode Alert"
url: "/wiki/Help\\:Shortcode_Alert"
description: ""
lead: "Display a alert box in the page."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Alert
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: Yes
    - type: label
      label: Shortcode
      value: "`alert`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display a message in a box. Either a predefined one, or written on the
        fly.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

This kind of shortcode is called a "nested shortcode". You can tell that it is
nested because it actually consists of two shortcodes with "something" in the
middle. It is just like HTML, where you have one tag (here shortcode), which
defines the beginning of a scope and one the end. Like in HTML the closing tag
uses the slash `/` before the designator to indicate an ending scope after this
point.

## Parameters

The post shortcode has the following parameters:

| Parameter | Optional                    | Description                                                     |
| --------- | --------------------------- | --------------------------------------------------------------- |
| `title`   | Yes                         | The title of the alert                                          |
| `type`    | Yes (defaults to `primary`) | The type of the alert (see section below)                       |
| `named`   | Yes                         | Use a predefined preset to set `title`, `type` (and "`.Inner`") |
| `.Inner`  | When not using `named`      | The actual text                                                 |

### Types

Depending on the type, the color and symbol in the front changes. This should
help the reader to understand the meaning of the alert. The type `danger` has
a red signal color and a exclamation mark as symbol, which transmits a
different priority and feeling than the `success` type, which green color
and a checkmark as symbol.

The `type` parameter can be one of the following:

| Type        | Description             |
| ----------- | ----------------------- |
| `primary`   | Primary alert (default) |
| `secondary` | Secondary alert         |
| `success`   | Success alert           |
| `danger`    | Danger alert            |
| `warning`   | Warning alert           |

### Presets

Some alerts are used quite often throughout the website. For those we created
presets that replace the `title`, context and `type` by predefined options.
This allows us to change them from a single location.

| Preset          | Description                                                                            |
| --------------- | -------------------------------------------------------------------------------------- |
| `coc`           | Tell the user to read the Code of Contact before continuing                            |
| `contrib-legal` | Tell the user, that only content done by the user will be accepted in an merge request |
| `todo`          | Tell the user, that this part of the website is not ready yet                          |
| `license-code`  | Tell the user, the source code is MIT licensed                                         |

## Examples

The most simple alert is one without a title and type. When no type
was set, the default type `primary` is used.

```md
{{</* alert */>}}
Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
sint consectetur cupidatat.
{{</* /alert */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< alert >}}
  Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
  sint consectetur cupidatat.
{{< /alert >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

An example for an `primary` alert would be this:

```md
{{</* alert title="Primary" type="primary" */>}}
Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
sint consectetur cupidatat.
{{</* /alert */>}}
```

In the rendered form it looks like the following.

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< alert title="Primary" type="primary" >}}
  Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
  sint consectetur cupidatat.
{{< /alert >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

Below, you see all types of alerts rendered. The `title` in those demo
alerts matches the `type` in title-case.

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< alert title="Primary" type="primary" >}}
  Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
  sint consectetur cupidatat.
{{< /alert >}}

{{< alert title="Secondary" type="secondary" >}}
  Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
  sint consectetur cupidatat.
{{< /alert >}}

{{< alert title="Success" type="success" >}}
  Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
  sint consectetur cupidatat.
{{< /alert >}}

{{< alert title="Danger" type="danger" >}}
  Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
  sint consectetur cupidatat.
{{< /alert >}}

{{< alert title="Warning" type="warning" >}}
  Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum
  sint consectetur cupidatat.
{{< /alert >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

Presets are rendered like this:

```md
{{</* alert preset="coc" */>}}{{</* /alert */>}}
{{</* alert preset="contrib-legal" */>}}{{</* /alert */>}}
{{</* alert preset="todo" */>}}{{</* /alert */>}}
{{</* alert preset="license-code" */>}}{{</* /alert */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
  {{< alert preset="coc" >}}{{< /alert >}}
  {{< alert preset="contrib-legal" >}}{{< /alert >}}
  {{< alert preset="todo" >}}{{< /alert >}}
  {{< alert preset="license-code" >}}{{< /alert >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/alert.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/alert.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Alert Shortcode" path="/layouts/shortcodes/alert.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_alert.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_alert.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Alert Style" path="/assets/scss/components/_shortcode_alert.scss" >}}
