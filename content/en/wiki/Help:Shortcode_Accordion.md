---
title: "Help:Shortcode Accordion"
url: "/wiki/Help\\:Shortcode_Accordion"
description: ""
lead: "Display content in vertically collapsing accordions."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Accordion
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: Yes
    - type: label
      label: Shortcode
      value: "`accordion` and `accordion_item`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Use the `accordion` shortcode, together with the `accordion_item`
        shortcode, to create collapsible containers which hide information
        behind an headers.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

`accordion_item` is a nested shortcode which produces vertically collapsing
accordions elements in an `accordion`.
To use accordion, you need two shortcodes `accordion` and `accordion_item`.

## Parameters

The Accordion shortcodes have the following parameters:

### accordion

The `accordion` is the outer wrapper for the `accordion_item` elements.
It is the part, which actually renders the elements.

| Parameter | Description                                    |
| --------- | ---------------------------------------------- |
| `html`    | if `false` (default), markdownify the content. |

### accordion_item

The `accordion_item` provides the outer wrapper `accordion` with all the
data needed to render the individual elements. A sole use of `accordion_item`
is not possible.

| Parameter | Description             |
| --------- | ----------------------- |
| `name`    | A name/label of the tab |

## Examples

```md
{{</* accordion */>}}

{{</* accordion_item name="A test" */>}}
This is just a test.
{{</* /accordion_item */>}}

{{</* accordion_item name="Another Test" */>}}
Just another Test.
{{</* /accordion_item */>}}

{{</* /accordion */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< accordion >}}
{{< accordion_item name="A test" >}}
This is just a test.
{{< /accordion_item >}}
{{< accordion_item name="Another Test" >}}
Just another Test.
{{< /accordion_item >}}
{{< /accordion >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

#### Accordion

Defined in `layouts/shortcodes/accordion.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/accordion.html" %}}

##### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Accordion Shortcode" path="/layouts/shortcodes/accordion.html" >}}

#### Accordion Item

Defined in `layouts/shortcodes/accordion_item.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/accordion_item.html" %}}

##### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Accordion Item Shortcode" path="/layouts/shortcodes/accordion_item.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_accordion.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_accordion.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Accordion Style" path="/assets/scss/components/_shortcode_accordion.scss" >}}
