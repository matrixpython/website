---
title: "Help:Workflow"
url: "/wiki/Help\\:Workflow"
description: "Contribute to our communtiy by improving our website."
lead: "Contribute to our communtiy by improving our website."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute"]
draft: false
images: ["gitlab-fork.png", "gitlab-fork-forms.png", "gitlab-clone.png"]
weight: 50
toc: true
---

You can contribute to our projects in two ways.

- You can report defect or suggest enhancements
- or solve issues, make changes or create content.

Either way, you start, by creating an issue in our
[issue tracker](https://gitlab.com/matrixpython/website/-/issues/new).

## Create an Issue

{{< alert preset="todo" >}}{{< /alert >}}

<!-- TODO: Use a real issue as example and use it in the MR tutorial -->

To standardize the way issues are created and to make that process easier, we
have created issue templates, you only need to fill out.

Our issue templates are:

{{< accordion >}}

{{< accordion_item name="Defect" >}}
You choose the `Defect` template, when you want to report:

- Bugs
- Vulnerabilities
- Rendering issues
- or other problems
  {{< /accordion_item >}}

{{< accordion_item name="Enhancement" >}}
You choose the `Enhancement` template, when you want to suggest:

- A new features
- Functional changes
- or any other improvements
  {{< /accordion_item >}}

{{< accordion_item name="Content - New" >}}
You choose the `Content - New` template, when you want to create a new page.
For example, you want to create a new:

- Wiki page
- Blog Post
- Documentation Page
  {{< /accordion_item >}}

{{< accordion_item name="Content - Edit" >}}
You choose the `Content - Edit` template, when you want to alter an existing
new page.

- by enhancing the wording,
- adding more information,
- fixing typos
  {{< /accordion_item >}}

{{< /accordion >}}
<br />
Please fill out as much information as you can.

When you handed in an Issue, you can follow it's status on that page.
To understand our triage process, check out the triage process help page.

{{< ln "wiki/Help:Triage_Process.md" >}}

## Create an Merge Request

If you want to go the extra mile, and try to solve issues with us, then you
want to create an <abbr title="A Pull Request, but on GitLab">Merge Request</abbr>.

### Prerequisites

We use `npm` as package manager for the project which deals with most of the
resources needed to work with and build the project. Unfortunately the
following packages are still needed.

- `npm` (Node.js)
- `git`
- `git-lfs`
- An [GitLab](https://gitlab.com) account.

### Workflow

{{< alert preset="contrib-legal" >}}{{< /alert >}}

The first step in working with repositories is to get them on your device.
But since you want to make changes, and integrate them in our repository, you
need to fork the repository first. To do that, open our project in your web
browser by clicking the following link.

<div class="text-center">
  <a href="https://gitlab.com/matrixpython/website" target="_blank" rel="noopener noreferrer">
    https://gitlab.com/matrixpython/website
  </a>
</div>

Click on `Fork` {{< dot 1 >}}, as shown in the image below, to create an fork
of the repository.

{{< img src="gitlab-fork.png" width=600 caption="Fork the Repository" >}}

Fill out the form. The important settings are `Namespace` {{< dot 1 >}}, which
should be set to your GitLab username and the `Visibility level` {{< dot 2 >}}
which must be `Public`.
If you like, you can change the `Project slug` to something else.
The combination of the `Project URL` and the `Project slug` will be
the URL, you can reach the project. In the image below the URL would be
[https://gitlab.com/MichaelSasser/website](https://gitlab.com/MichaelSasser/website).

When you are done with the form click on the blue button
`Fork project` {{< dot 3 >}}.

{{< img src="gitlab-fork-forms.png" width=600 caption="Fill out the form" >}}

After waiting a few seconds, the website should be forked, and you see the
message "The project was successfully forked".
This will be your `origin` repository you make changes to.

The next step is to clone the website to your device. On the right side you
find a blue button `Clone` {{< dot 1 >}}. Click on the button. A new popover
will appear.
In the popover, below "Clone with SSH", click on the clipboard symbol
{{< dot 2 >}}.
This will copy the text left to it, "git@gitlab.com:[...]", in your devices clipboard.

{{< img src="gitlab-clone.png" width=400 caption="Fill out the form" >}}

Now open a virtual terminal or a Powershell window and navigate to your
preferred path, you want the to be cloned to. Clone the repository by
writing `git clone` and pasting the text from your clipboard. It should now
look something like
`git clone git@gitlab.com:MichaelSasser/website.git`.
If you like, you prefer to give the local copy of your repository another name
you can add that name as another positional argument. For example:
`git clone git@gitlab.com:MichaelSasser/website.git mp-website`.
Press the enter key on your keyboard.

```bash
$ git clone git@gitlab.com:MichaelSasser/website.git mp-website
Cloning into 'mp-website'...
remote: Enumerating objects: 5235, done.
remote: Counting objects: 100% (2270/2270), done.
remote: Compressing objects: 100% (733/733), done.
remote: Total 5235 (delta 1460), reused 2270 (delta 1460), pack-reused 2965
Receiving objects: 100% (5235/5235), 4.87 MiB | 7.77 MiB/s, done.
Resolving deltas: 100% (3153/3153), done.
Filtering content: 100% (38/38), 11.72 MiB | 2.79 MiB/s, done.
```

`git` is now cloning your repository.

If you left the name as it is, the newly created directory has the name you
set in the `slug`, for example `website`. Otherwise, it has the
name you gave it when you run `git clone`, like in the second example
`mp-website`. In the following we refer to it as `mp-website`.

During the cloning step `git-lfs` might, or
might not have also pull and populate the working copy with real content
from Git LFS files.
That usually depends on your `~/.gitconfig`, the versions of `git`
and `git-lfs` you use and sometimes even in which mood `git` is.

To check right away if `git-lfs` pulled the LFS content, navigate to your
repository's directory with `cd mp-website` and check what is inside an image.
For example the image `./static/android-chrome-192x192.png`.

```bash
$ cat static/android-chrome-192x192.png
oid sha256:2178ba28946659c01455efca2171be2ab77afafbf052ee83f567a2f283007fc4
size 23505
```

If the output of the command looked something like this and not like your
virtual terminal needs an exorcism, `git-lfs` is either not installed or it
did not clone and populate the LFS content in the repository.
If your virtual terminal shows gibberish, `git-lfs` is installed and it did
populate the LFS content. You can get rid of the weird stuff in the terminal
by running `reset`. Just type it and press enter.

If you didn't need an exorcist for your terminal, you need to tell `git-lfs`
to pull the LFS content and populate it. Run:

```bash
# Ensure git hooks are set-up
$ git-lfs install
Updated Git hooks.
Git LFS initialized.

# Pull the LFS content and populate the working copy with actual content
$ git-lfs pull
Downloading LFS objects: 100% (38/38), 12 MB | 4.0 MB/s
```

When your shell responded with something like
`zsh: command not found: git-lfs`. you need to install `git-lfs` first.

Now it is time to install the dependencies with `npm`. Make sure you are
in at the project root (the `mp-website` directory) and run:

```bash
$ npm install

> matrixpython/website@0.1.0 postinstall
> hugo-installer --version otherDependencies.hugo --extended --destination node_modules/.bin/hugo


Hugo Installer

> Checking for existing binary
> Downloading binary from "https://github.com/gohugoio/hugo/releases/download/v0.101.0/hugo_extended_0.101.0_Linux-64bit.tar.gz"
> Downloading checksum from "https://github.com/gohugoio/hugo/releases/download/v0.101.0/hugo_0.101.0_checksums.txt"
> Verifying binary checksum
> Extracting binary to disk
> Verifying binary health

Hugo is now available in "node_modules/.bin/hugo".

- Version       0.101.0
- Extended      Yes
- OS            linux
- Architecture  x64

hugo v0.101.0-466fa43c16709b4483689930a4f9ac8add5c9f66+extended linux/amd64 BuildDate=2022-06-16T07:09:16Z VendorInfo=gohugoio

Success!

added 620 packages, and audited 621 packages in 27s

93 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

Yes, you just installed 600+ packages into the `./node_modules/` of the
project root. But don't be shocked. In the JavaScript world, packages might
just be a couple of lines of code. This is pretty normal. Most of the packages
are dependencies of dependencies and sometimes those in different versions.
Nothing to worry about. You see, this ain't Python.

Now everything should be set-up correctly and you can start implementing
features or resolving issues.

If you want to implement a feature, which currently has no open issue, you
are required to create one yourself, before you can continue. We
don't accept any merge requests without an issues.
At the beginning of the page, is described you can
[create an issue]({{< relref "#create-an-issue" >}}).

In this little tutorial, we assume, an issue already exists, we want to
resolve. As issue, we use the one from the issue tutorial TBD:

{{< alert preset="todo" >}}{{< /alert >}}

<!-- TODO: -->

```bash
$ npm run dev

> matrixpython/website@0.1.0 dev
> exec-bin node_modules/.bin/hugo/hugo server --bind=0.0.0.0 --disableFastRender --gc

Start building sites …
hugo v0.101.0-466fa43c16709b4483689930a4f9ac8add5c9f66+extended linux/amd64 BuildDate=2022-06-16T07:09:16Z VendorInfo=gohugoio

                   | EN
-------------------+------
  Pages            | 258
  Paginator pages  |  14
  Non-page files   |  41
  Static files     |  80
  Processed images | 200
  Aliases          |  57
  Sitemaps         |   1
  Cleaned          |   0

Built in 15318 ms
Watching for changes in /home/michael/myrepos/mp-website/{archetypes,assets,babel.config.js,content,data,i18n,layouts,node_modules,package.json,static}
Watching for config changes in /home/michael/myrepos/mp-website/config/_default, /home/michael/myrepos/mp-website/config/_default/menus, /home/michael/myrepos/mp-website/config/development, /home/michael/myrepos/mp-website/config/development/menus
Environment: "development"
Serving pages from memory
Web Server is available at http://localhost:1313/ (bind address 0.0.0.0)
Press Ctrl+C to stop
```

```bash
$ npm run lint

> matrixpython/website@0.1.0 lint
> npm run -s lint:scripts && npm run -s lint:styles && npm run -s lint:markdown

markdownlint-cli2 v0.5.1 (markdownlint v0.26.2)
Finding: content/**/*.md !node_modules !CHANGELOG.md
Linting: 113 file(s)
Summary: 0 error(s)
```

```bash
$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
 modified:   .markdownlint-cli2.jsonc
 modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```

```bash
$ git commit -m "feat(shortcode): Add dot shortcode"

[main 9465880] feat(shortcode): Add dot shortcode
 6 files changed, 211 insertions(+), 1 deletion(-)
 create mode 100644 assets/scss/components/_shortcode_dot.scss
 create mode 100644 content/en/wiki/Help:Shortcode_Dot.md
 create mode 100644 layouts/shortcodes/dot.html
```

```bash
$ git push
Uploading LFS objects: 100% (3/3), 394 KB | 0 B/s, done.
Enumerating objects: 28, done.
Counting objects: 100% (28/28), done.
Delta compression using up to 8 threads
Compressing objects: 100% (16/16), done.
Writing objects: 100% (17/17), 5.37 KiB | 785.00 KiB/s, done.
Total 17 (delta 9), reused 0 (delta 0), pack-reused 0
To gitlab.com:matrixpython/website.git
   8ddcebb..801a7f5  main -> main
```

- Add this repository as a remote named `upstream`.
- Create a new branch from the
  [main](https://gitlab.com/matrixpython/website/-/tree/main)
  branch. Let's say your issue was issue `#42` and you want to create a
  feature. Your branch name would be `feature/#42-my-feature`.
- Install the required tools with `npm install` and serve the website on your
  machine with `npm run dev`.
- (Optional) Use `./utils/get_matrix_rooms <homeserver>`, e.g.
  `./utils/get_matrix_rooms matrix.org` to render the user counter and
  activate some features.
- (Optional) Use `./utils/get_matrix_users <homeserver> <your matrix session token>`, e.g.
  `./utils/get_matrix_users matrix.org "asdfoobarbaz"` to render the matrix
  user pills with avatars and activate some features.
- (Moderators only) Pull in the internal documentation using:
  `git clone git@gitlab.com:matrixpython/website-internal.git content/en/internal`.
  Access to this repository and the internal section are restricted.
- Open the URL `[http://localhost:1313](http://localhost:1313)` in your web
  browser. You should now see the website.
- Implement the feature (or bugfix) you described in your issue. The website
  in your browser will update as soon as you save the file you are editing.
- Commit and publish your branch to your fork (`origin`).
- Create a `Pull Request` from the branch, which contains your changes to
  this repository's `master` branch.
- Once the pull request is reviewed and merged, you can pull the changes from
  `upstream` (this repository) to your local repository and start over again
  from step 5. **Don't forget to create an issue first.**

{{< alert preset="coc" >}}{{< /alert >}}

We recommend to use the
[GLab - GitLab CLI tool](https://github.com/profclems/glab).

## About the Website

The repository has one branch with infinite lifetime:

- The
  [main](https://gitlab.com/matrixpython/website/)
  branch -- the website reflects the current state of the main branch. When
  it gets changed, GitLab CI automatically builds and deploys the website.

When you are working with the repo, make sure to follow the
[GitLab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html).

The Website is built with [Hugo](https://gohugo.io/) and based on the lovely
template [Doks](https://getdoks.org/), by
[Henk "h-enk" Verlinde](https://github.com/h-enk). You will find useful
information you may need in their
[documentation](https://getdoks.org/docs/prologue/introduction/),
[code](https://github.com/h-enk/doks) or
[discussions](https://github.com/h-enk/doks/discussions) of the `Doks`
template. Please keep in mind, their discussions and issues are for the
template they made and not for our project.

## Troubleshooting

{{< accordion >}}

{{< accordion_item name="Git asked me for a username and email address instead" >}}
TBD
{{< /accordion_item >}}

{{< accordion_item name="Git LFS did not populate the working copy with real content" >}}
TBD
{{< /accordion_item >}}

{{< accordion_item name="Git asked me for the merging method" >}}
TBD
{{< /accordion_item >}}

{{< accordion_item name="Git Could not read from remote repository" >}}
TBD: wrong repo / ssh pubkey not added to gl / no ssh pub key

```bash
Cloning into 'mp-website'...
Host key verification failed.
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

{{< /accordion_item >}}

{{< accordion_item name="'lfs' is not a git command" >}}
TBD: install git-lfs

If the output of the following command looks something like that and not like
that:

```bash
$ cd /path/to/your/repo
$ cat static/android-chrome-192x192.png
oid sha256:2178ba28946659c01455efca2171be2ab77afafbf052ee83f567a2f283007fc4
size 23505
```

and not like your virtual terminal needs an exorcism, `git-lfs` is not, or at
least was not installed, at the time, you cloned your repository.
(If your virtual terminal shows gibberish, you can fix is by running `reset`)

```bash
$ git-lfs install
Updated Git hooks.
Git LFS initialized.
$ git-lfs pull
Downloading LFS objects: 100% (38/38), 12 MB | 4.0 MB/s
```

{{< /accordion_item >}}

{{< /accordion >}}
