---
title: "Help:Shortcode Download"
url: "/wiki/Help\\:Shortcode_Download"
description: ""
lead: "Let users download a file."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Download
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`download`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display a download box with metadata information, such as as a title,
        the file size and checksums in md5, sha1 and sha256.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

To allow a user to download a file, you need to create a file either the
directory, your page is located (make sure you create a page bundle) or
anywhere else in project, relative to the project root. If you create the
file in the `static` directory, for example `./static/favicon.svg`, the
path parameter must be prefixed with an `/`, followed by relative path from
there, and the file name. The path in this case would be
`/static/favicon.svg`. If the file `your_file.py` is in your page bundle,
which might look like this one, where your page is `index.md`:

```bash
# tree of ./content/en/docs/contributing/
...
├── contributing/
│   ├── shortcodes/
│   │   ├── index.md
│   │   └── hello_world.py
│   └── _index.md
└── _index.md
```

you would use the path `your_file.py`. To give the file a title, use the
`title` parameter.

The file size and checksums are created automatically for you.

{{< alert type="success" title="Filetypes" >}}
When you want to commit a big or non human readable file, like a binary, which
wouldn't typically committed with git, you must ensure the file is tracked
by `git-lfs` instead of `git`. To do that, use `git lfs track "*.ytd"`.
<br />
<br />
To see, which file extensions are currently tracked by `git-lfs` run
`git lfs track`.
{{< /alert >}}

## Parameters

The download shortcode has the following parameters:

| Parameter | Optional | Description                                                                                                     |
| --------- | -------- | --------------------------------------------------------------------------------------------------------------- |
| `path`    | No       | The path to the file, either relative from your page bundle or with the `/`-prefix relative to the project root |
| `title`   | Yes      | An optional title for the file                                                                                  |

## Examples

```md
<!-- Without title and a path relative to the project roots static directory -->

{{</* download path="/static/favicon.svg" */>}}

<!-- Same as the first, but with a title -->

{{</* download title="Our Favicon" path="/static/favicon.svg" */>}}

<!-- With a title and relative to the page-bundle -->

{{</* download title="Page-Bundle Example" path="hello_world.py" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}

{{< download path="/static/favicon.svg" >}}
{{< download title="Our Favicon" path="/static/favicon.svg" >}}
{{< download title="Page-Bundle Example" path="hello_world.py" >}}

{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in: `/layouts/shortcodes/download.html`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/download.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Download Shortcode" path="/layouts/shortcodes/download.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_download.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_download.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Download Style" path="/assets/scss/components/_shortcode_download.scss" >}}
