---
title: "Help:Shortcode Tabs"
url: "/wiki/Help\\:Shortcode_Tabs"
description: ""
lead: "Display content in tabs."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Tabs
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: Yes
    - type: label
      label: Shortcode
      value: "`tabs` and `tab`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Use the `tabs` shortcode, together with the `tab` shortcode, to
        create clickable tabs to switch out content.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

`tabs` is a nested shortcode which produces a tabbed view. To use tabs, you
accordion. To use tabs, you
need two shortcodes `tabs` and `tab`.

## Parameters

The GitLab Label shortcode has the following parameters:

### Tabs

| Parameter | Description                                              |
| --------- | -------------------------------------------------------- |
| `name`    | A namespace to let Hugo distinguish them from each other |

### Tab

| Parameter  | Description                                            |
| ---------- | ------------------------------------------------------ |
| `name`     | A name/label of the tab                                |
| `codelang` | (optional) Render code blocks with syntax highlighting |

## Examples

```md
{{</* tabs name="some-name" */>}}

{{</* tab name="Python" codelang="python" */>}}
print("Hello World")
{{</* /tab */>}}

{{</* tab name="C" codelang="c" */>}}
printf("Hello World");
{{</* /tab */>}}

{{</* tab name="Description" */>}}
This is just a description. No code gets rendered.
{{</* /tab */>}}}

{{</* /tabs */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< tabs name="some-name" >}}
{{< tab name="Python" codelang="python" >}}
print("Hello World")
{{< /tab >}}
{{< tab name="C" codelang="c" >}}
printf("Hello World")
{{< /tab >}}
{{< tab name="Description" >}}
This is just a description. No code gets rendered.
{{< /tab >}}}
{{< /tabs >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

#### Tabs

Defined in `layouts/shortcodes/tabs.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/tabs.html" %}}

##### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Tabs Shortcode" path="/layouts/shortcodes/tabs.html" >}}

#### Tab

Defined in `layouts/shortcodes/tab.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/tab.html" %}}

##### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Tab Shortcode" path="/layouts/shortcodes/tab.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_accordion.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_tabs.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Tabs Style" path="/assets/scss/components/_shortcode_tabs.scss" >}}
