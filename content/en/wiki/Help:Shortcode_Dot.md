---
title: "Help:Shortcode Dot"
url: "/wiki/Help\\:Shortcode_Dot"
description: ""
lead: "Display a red circle around a number."
date: 2022-08-11T09:43:00+02:00
lastmod: 2022-08-11T09:43:00+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Dot
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`dot`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display a red circle around a number.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

Use the shortcode `dot` to display a filled red circle with a white number in
it. This is highly useful for documenting steps shown in an image, with
the same looking kind of incremental labels.

## Parameters

The Ln shortcode has the following parameters:

| Parameter | Description                                                |
| --------- | ---------------------------------------------------------- |
| `[0]`     | The number in the label as signed integer between `-9..99` |

## Examples

```md
Do foo as described in {{</* dot 1 */>}} on the image.
See also {{</* dot 42 */>}} or {{</* dot -2 */>}}.
```

<!-- prettier-ignore-start -->
{{< rendered >}}
Do foo as described in {{< dot 1 >}} on the image.
See also {{< dot 42 >}} or {{< dot -2 >}}.
{{< /rendered >}}

<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/dot.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/dot.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Details Shortcode" path="/layouts/shortcodes/dot.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_dot.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_dot.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Details Style" path="/assets/scss/components/_shortcode_dot.scss" >}}
