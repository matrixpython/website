---
title: "Help:Shortcode Readfile"
url: "/wiki/Help\\:Shortcode_Readfile"
description: ""
lead: "Render a code-block from a file."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Readfile
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: Markdown Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`readfile`"
    - type: label
      label: Return Type
      value: Markdown
    - type: fulldata
      label: Short Description
      value: |
        Render a file relative to the project root as Markdown code block.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

The `readfile` shortcode is used to render a code block from a source code
file. On this page the shortcode is used to render the code, you see below.

## Parameters

The post shortcode has the following parameters:

| Parameter | Description                                                                          |
| --------- | ------------------------------------------------------------------------------------ |
| `path`    | The absulute path, relative to the project root, to the file to render as code block |
| `type`    | Overwrite the file type used for highlighting.                                       |

<!-- prettier-ignore-start -->
{{< alert title="File Type" >}}
By default the extension of the file is used to derive the file type.
{{< /alert >}}
<!-- prettier-ignore-end -->

## Examples

### Type derived from file extension

```md
{{%/* readfile path=".prettierrc.yaml" */%}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{% readfile path=".prettierrc.yaml" %}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

### Type overwritten

```md
{{%/* readfile path="theme.toml" type="toml" */%}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{% readfile path="theme.toml" type="toml" %}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/readfile.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/readfile.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Readfile Shortcode" path="/layouts/shortcodes/readfile.html" >}}

### JavaScript

Defined in: `/assets/js/highlight.js`<br />
Uses: `highlight.js` (Package: [NPM](https://www.npmjs.com/package/highlight.js), License: [BSD-3-Clause](https://github.com/highlightjs/highlight.js/blob/main/LICENSE))

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/js/highlight.js" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Highlight Script" path="/assets/js/highlight.js" >}}
