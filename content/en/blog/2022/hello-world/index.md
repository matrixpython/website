---
title: "Hello World 👋"
description: "Introducing our new documentation websites."
lead:
  "Welcome to the Python Community on Matrix website. As hobbyists and
  developers, we know how useful documentations are. When we joined the Matrix
  network, we felt a bit lost too. With this website, we try to help newcomers
  to get started."
date: 2022-06-22T16:30:00+02:00
lastmod: 2022-06-22T16:30:00+02:00
draft: false
weight: 50
images: []
contributors: ["Michael Sasser"]
categories: ["website"]
tags: ["announcement", "docs", "blog"]
---

{{< alert preset="todo" >}}{{< /alert >}}

<!--
  TODO: Add more content and describe, what is on the page and how to find it.
-->
