---
title: "Join Spam Attack Update 4"
description: ""
lead: "This kick-wave should hopefully be the last one."
date: 2022-05-17T12:19:00+02:00
lastmod: 2022-05-17T12:19:00+02:00
draft: false
weight: 50
contributors: ["Michael Sasser"]
categories: ["Community", "Matrix"]
tags: ["Announcement"]
images: []
header_image: ""
floating_image: ""
floating_image_width: 40
floating_image_position: "right"
floating_image_caption: ""
---

We assume, we get it through today, with the last ban wave. We think, we have
no false-positives in our kick-list, but, just in case, you can join again,
immediately, if you get kicked (You are safe, when joined before Thu 28 April
2022 15:19:37 UTC).

Since we don't rate limit the kicks anymore, this runs at
a much higher rate on a beefed up homeserver. So, the room will become worse
before it gets better.
