---
title: "Domain, Email and CDN"
date: 2023-03-19T13:00:01+00:00
lastmod: 2023-03-19T13:01:00+00:00
contributors: ["Michael Sasser"]
draft: false
---

It was quite tiresome to use placeholders, code names and personal email
addresses for our projects especially when laying out APIs. So we scored
`matrixpython.org` as our domain. With that out of the way, we can make the
code names permanent and settle on [matrixpython.org](matrixpython.org) and
[www.matrixpython.org](www.matrixpython.org)
are for our new website. {{< identifier "info@matrixpython.org" >}} is now our
general purpose email address and {{< identifier "abuse@matrixpython.org" >}}
shall be for reporting things. The website is now hosted using the Cloudflare
Pages and the Cloudflare CDN. It is now incredible fast and allows us to
protect our internal pages using our own OpenID Connect provider.
