---
title: "defnull Joined the Moderation Team"
date: 2022-11-10T17:48:00+01:00
lastmod: 2022-11-10T17:48:00+01:00
contributors: ["Michael Sasser"]
draft: false
---

After helping countless users, [defnull]({{< ref "contributors/defnull" >}})
finally joined the moderation team.
