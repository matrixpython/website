---
title: "The Abuse Management Bot was renamed to Administrator"
date: 2022-03-14T13:00:00+00:00
lastmod: 2022-03-14T13:00:00+00:00
contributors: ["Michael Sasser"]
draft: false
---

The Abuse Management bot (Mjölnir) was renamed to
[Administrator]({{< ref "contributors/mjolnir" >}}) like in the official
matrix rooms before. This should make it easier to understand it's purpose for
new users.
