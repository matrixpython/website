---
title: "The Abuse Management Bot Joined the Moderation Team"
date: 2021-04-30T05:44:00+00:00
lastmod: 2021-04-30T05:44:00+00:00
contributors: ["Michael Sasser"]
draft: false
---

The [Abuse Management]({{< ref "contributors/mjolnir" >}}) bot
(now known as "Administrator" or "Mjölnir") was
deployed to the server. After a few days of intense testing, the bot joined the
Python room and started protecting the users from spam. Since then the bot
watches our rooms room 24/7.
