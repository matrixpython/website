---
title: "Move Website Development to GitLab"
date: 2022-04-12T22:03:31+02:00
lastmod: 2022-04-12T22:03:31+02:00
contributors: ["Michael Sasser"]
draft: false
---

Originally, we decided to use Git LFS for images and static content in
binary form, which doesn't fit in the typical versionable category.
With some experience of not using Git LFS in scenarios like this,
one might conclude, that this is a reasonable use case for using
this extension.

While developing the website, we ran into issues with GitHub's LFS policy.
The advertised 1 GB of storage and bandwidth per account/company does
not mean, monthly. The quota does not reset by itself. Only by throwing
credits at it, we were able to continue development. But that wasn't the
worst thing at all.
Every use of that quota is metered. Even pulling the LFS content in
GitHub Actions. A fully integrated service of GitHub.
This was not acceptable for us. So - we decided to try out
alternatives.

In the end, we decided to go with GitLab. It doesn't meter Git LFS
bandwidth usage at all. Even the storage use doesn't have a quota
by itself. It counts as user space together with artifacts and
anything else stored on GitLab by that project.
