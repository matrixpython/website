---
title: "Heimdall, the Next-Generation Moderation Bot"
date: 2023-02-04T13:04:54+01:00
lastmod: 2023-02-04T13:04:54+01:00
contributors: ["Michael Sasser"]
draft: false
---

We have just created the initial commit, writing our own moderation bot for Matrix using the
[Rust Matrix Appservice SDK](https://github.com/matrix-org/matrix-rust-sdk).
<br /><br />
It will be a feature rich, multi-tenant bot, that intends to fix numerous
issues we are facing with the current bot situation. It will use machine
learning, statistical analysis and heuristics to fight spam more accurately.
Most of the bot actions you presently might see happening is us manually
giving the bot commands. We want our bot to handle more incidents themselves
and empower our users to support its decision making progress without efforts.
Also, the flooding protection will not ban anymore for just typing
fast without violating the default federation rate limit for users. Overall
the bot will not be as aggressive against users as our current solution is.
Even bans might not be permanent anymore.<br />
So, stay tuned!
